# coding=utf8
'''
Created on May 15, 2016

@author: dell
'''
def ngram(word, n):
    wordlist = []
    if len(word)>=n:
            for i in range(len(word)-(n-1)):
                wordlist.append(word[i:i+n])
        
            return wordlist
            
a="paraparaparadise"
b="paragraph"

x=set(ngram(a, 2))
y=set(ngram(b, 2))

print x.union(y)
print x.intersection(y)
print x.difference(y)



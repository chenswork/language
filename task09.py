# coding=utf8
'''
Created on May 15, 2016

@author: dell
'''
import random

def typoglycemia(words):
    goal = ""
    words = words.replace('.', ' .')
    words = words.replace(',', ' ,')
    words = words.split()
    for word in words: 
        if len(word) > 4:
            list = []
            for c in word:
                list.append(c)
            del list[0]
            list.pop()
            random.shuffle(list)
            list.insert(0, word[0])
            list.append(word[-1])
            word=''.join(list)
        goal += (word +' ') 
    goal = goal.replace(' .', '.')
    goal = goal.replace(' ,', ',')
    return goal

a="I dont have a pencil.You havent a pencil."
a = typoglycemia(a)
print a
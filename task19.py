# coding=utf8
'''
Created on May 23, 2016

@author: dell
'''
from collections import defaultdict

prefectures = defaultdict(int)

with open("hightemp.txt") as f:
    line = f.readline()
    while line:
        prefectures[line.split()[0]] += 1
        line = f.readline()

for k, v in sorted(prefectures.items(), key=lambda x: x[1], reverse=True):
    print(k)
